import { Answer, generate_Random } from "./model.js"

let generateRandom = 0
let initialPlay = false;
let answer = new Answer();

document.querySelector("#play").addEventListener("click", () => {

    if (initialPlay == false) {
        initialPlay = true
        generateRandom = generate_Random()
        console.log(generateRandom)
        document.querySelector("#play").disable = true
        alert("El juego ha iniciado")

    } else {
        alert("Si quiere otro numero reinice la pagina")
    }

})

document.querySelector("#v").addEventListener("click", () => {

    let res = document.querySelector("#num").value
    let resOne = 0;

    for(let i = 0; i<  res.length; i++){

        for(let j = i+1; j< res.length; j++){

            if(i != j){
                 
                if(res[i] == res[j]){
                    console.log(res[i], res[j])
                    resOne++
                }
            }
        }
    }
     console.log(resOne)
    if (res.length !== 4 || generateRandom == 0 || resOne !== 0) {
        alert("Ingrese bien el numero, son cuatro digitos o haga click en el boton play para poder jugar")
    }
    else {
        answer.numTrying()
        answer.setnumUser(res)

        for (let i = 0; i < res.length; i++) {

            for (let r = 0; r < generateRandom.length; r++) {

                if (res[i] == generateRandom[r]) {

                    if (i == r) {
                        answer.numFixed()

                    } else if (i != r) {

                        answer.numSpades()
                    }
                }
            }
        }

        if (answer.getSuccesfull() == true) {

            document.querySelector("tbody").innerHTML += `   
                <tr class="table-success">
                <th scope="row">${answer.trying}</th>
                <td>${answer.numUser}</td>
                <td>${answer.spades}</td>
                <td>${answer.fixed}</td>
                </tr>`
            answer.getReset()
            initialPlay = false;
            document.querySelector("#v").disable = false

            setTimeout(function () {
                document.querySelector("tbody").innerHTML = ''
            }, 5000);



        } else {

            document.querySelector("tbody").innerHTML += `   
                <tr class="table-danger">
                <th scope="row">${answer.trying}</th>
                <td>${answer.numUser}</td>
                <td>${answer.spades}</td>
                <td>${answer.fixed}</td>
                </tr>`

            answer.getError()
        }

    }
    document.querySelector("#num").value = ''
})

