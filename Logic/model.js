export class Answer {

    constructor() {

        this.trying = 0;
        this.spades = 0;
        this.fixed = 0;
        this.numUser = 0;
    }

    setnumUser(numUser){
        this.numUser = numUser
    }
    
    getnumUser(){
        return this.numUser
    }

    numTrying(){
        this.trying++
        return this.trying
    }

    numSpades(){
        this.spades++
        return this.spades
    }

    numFixed(){
        this.fixed++
        return this.fixed
    } 

    getSuccesfull() {
        if (this.fixed == 4) {
            
            alert("Ganaste")
            
            return true
        }
            return false

    }

    getError(){
        this.spades = 0
        this.fixed = 0
        this.numUser = 0
    }

    getReset(){
        this.trying = 0
        this.spades = 0
        this.fixed = 0
        this.numUser = 0
    }

}

export function generate_Random(){
    let randomRight = 1
    let generateRandom = 0

    while (randomRight !== 2) {
        let min = Math.ceil(1000)
        let max = Math.floor(9999)
        generateRandom = Math.floor(Math.random() * (max - min + 1)) + min
        generateRandom = generateRandom.toString()
        for (let i = 0; i < generateRandom.length; i++) {
            for (let j = i + 1; j < generateRandom.length; j++) {
                if (generateRandom[i] == generateRandom[j]) {
                    randomRight++
                }
            }
        }
        if (randomRight == 1) {
            randomRight = 2
        } else {
            randomRight = 1
        }

    }
    return generateRandom
}